#!/bin/bash

## voxl-cross contains the following toolchains
## first two for apq8096, last for qrb5165
TOOLCHAIN_APQ8096_32="/opt/cross_toolchain/arm-gnueabi-4.9.toolchain.cmake"
TOOLCHAIN_APQ8096_64="/opt/cross_toolchain/aarch64-gnu-4.9.toolchain.cmake"
TOOLCHAIN_QRB5165="/opt/cross_toolchain/aarch64-gnu-7.toolchain.cmake"

# specific flags for opencv
COMMON_FLAGS="\
		-DOPENCV_EXTRA_MODULES_PATH=../opencv_contrib/modules \
		-DCMAKE_BUILD_TYPE=RELEASE \
		-DBUILD_opencv_gapi=OFF \
		-DWITH_OPENCL=ON \
		-DHAVE_OPENCL=1 \
		-DCMAKE_INSTALL_PREFIX=../pkg/data/usr \
		-DOPENCL_INCLUDE_DIR=../opencl_includes \
		-DENABLE_NEON=ON \
		-DBUILD_TESTS=OFF \
		-DBUILD_PERF_TESTS=OFF \
		-DBUILD_EXAMPLES=OFF \
		-DBUILD_opencv_apps=OFF \
		-DPYTHON_DEFAULT_EXECUTABLE=/usr/bin/python3.6 \
		-DPYTHON3_INCLUDE_DIR=/usr/include/python3.6m \
		-DPYTHON3_LIBRARY=/usr/lib/aarch64-linux-gnu/libpython3.6m.so \
		-DBUILD_NEW_PYTHON_SUPPORT=ON \
		-DBUILD_opencv_python3=ON \
		-DHAVE_opencv_python3=ON \
		-DBUILD_opencv_python=OFF \
		-DHAVE_opencv_python=OFF \
		-DINSTALL_PYTHON_EXAMPLES=ON \ 
		-DWITH_GTK=OFF"


set +e
echo "Applying Patches"
## TODO, patch not needed on 865 maybe?
patch -uN opencv/modules/core/src/ocl.cpp -i patches/workgroup_size.patch
echo "Done Applying Patches"
set -e

## this list is just for tab-completion
AVAILABLE_PLATFORMS="qrb5165 apq8096 native"


print_usage(){
	echo ""
	echo " Build the current project based on platform target."
	echo ""
	echo " Usage:"
	echo ""
	echo "  ./build.sh apq8096"
	echo "        Build 64-bit binaries for apq8096"
	echo ""
	echo "  ./build.sh qrb5165"
	echo "        Build 64-bit binaries for qrb5165"
	echo ""
	echo "  ./build.sh native"
	echo "        Build with the native gcc/g++ compilers."
	echo ""
	echo ""
}

case "$1" in
	apq8096)
		## multi-arch library, build both 32 and 64 for apq8096
		EXTRA_FLAGS="-march=armv8-a -DCV__EXCEPTION_PTR=0 -L  /usr/aarch64-linux-gnu-2.23/lib -I  /usr/aarch64-linux-gnu-2.23/include"
		BUILD_DIR="build64"

		mkdir -p ${BUILD_DIR}
		cd ${BUILD_DIR}

		cmake \
			-DCMAKE_TOOLCHAIN_FILE=${TOOLCHAIN_APQ8096_64} \
			${COMMON_FLAGS} \
			-DBUILD_ZLIB=ON \
			-DCMAKE_CXX_FLAGS="${CMAKE_CXX_FLAGS} ${EXTRA_FLAGS}"\
			-DCMAKE_INSTALL_LIBDIR="lib64"\
			-DPYTHON_DEFAULT_EXECUTABLE=/usr/bin/python2.7 \
			../opencv/

		make -j$(nproc)
		cd ..
		;;
	qrb5165)
		EXTRA_FLAGS="-march=armv8.2-a -DCV__EXCEPTION_PTR=0" 
		BUILD_DIR="build"

		mkdir -p ${BUILD_DIR}
		cd ${BUILD_DIR}

		cmake \
			-DCMAKE_TOOLCHAIN_FILE=${TOOLCHAIN_QRB5165} \
			${COMMON_FLAGS} \
			-DBUILD_ZLIB=ON \
			-DCMAKE_CXX_FLAGS="${CMAKE_CXX_FLAGS} ${EXTRA_FLAGS}"\
			-DCMAKE_INSTALL_LIBDIR="lib64"\
			-DPYTHON_DEFAULT_EXECUTABLE=/usr/bin/python2.7 \
			-DPYTHON3_INCLUDE_DIR=/usr/include/python3.6m \
			-DPYTHON3_LIBRARY=/usr/lib/aarch64-linux-gnu/libpython3.6m.so \
			-DPYTHON2_INCLUDE_DIR=/usr/include/python2.7 \
			-DPYTHON2_LIBRARY=/usr/lib/aarch64-linux-gnu/libpython2.7.so \
			-DBUILD_NEW_PYTHON_SUPPORT=ON \
			-DBUILD_opencv_python3=ON \
			-DHAVE_opencv_python3=ON \
			-DBUILD_opencv_python=ON \
			-DHAVE_opencv_python=ON \
			../opencv/

		make -j$(nproc)
		cd ..
		;;

	native)
		mkdir -p build
		cd build
		cmake ${COMMON_FLAGS} ../opencv/
		make -j4
		cd ../
		;;

	*)
		print_usage
		exit 1
		;;
esac


